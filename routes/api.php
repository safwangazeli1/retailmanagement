<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\Api\SalaryController;
use App\Http\Controllers\Api\ExpenseController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\CustomerController;
use App\Http\Controllers\Api\EmployeeController;
use App\Http\Controllers\Api\SupplierController;

Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login',  [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::post('me', [AuthController::class, 'me']);
    Route::post('signup', [AuthController::class, 'signup']);
    
});

Route::ApiResource('/supplier', SupplierController::class);

Route::ApiResource('/category', CategoryController::class);

Route::ApiResource('/product', ProductController::class);

Route::ApiResource('/employee', EmployeeController::class);

Route::ApiResource('/expense', ExpenseController::class);

Route::ApiResource('/customer', CustomerController::class);

Route::Post('/salary/paid/{id}', [ SalaryController::class, 'Paid']);
Route::Get('/salary', [ SalaryController::class, 'AllSalary']);
Route::Get('/salary/view/{id}', [ SalaryController::class, 'ViewSalary']);

Route::Get('/edit/salary/{id}', [ SalaryController::class, 'EditSalary']);
Route::patch('/salary/update/{id}', [ SalaryController::class, 'SalaryUpdate']);
